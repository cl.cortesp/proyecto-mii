from django.db import models

# Create your models here.

#python manage.py makemigrations ---> lee el archivo models y crea un archivo de migracion
#python manage.py migrate ---> toma las migraciones y las hace en BBDD

class Tipo(models.Model):
    tipo = models.CharField(max_length=80)
    def __str__(self):
        return self.tipo

class Especie(models.Model):
    nombre = models.CharField(max_length=100)
    def __str__(self):
        return self.nombre

class Sexo(models.Model):
    sexo = models.CharField(max_length=20)
    def __str__(self):
        return self.sexo

class Color(models.Model):
    nombre = models.CharField(max_length=100)
    def __str__(self):
        return self.nombre        
    

class Mascota(models.Model):
    nombre = models.CharField(max_length=200)
    tipo = models.ForeignKey(Tipo, on_delete=models.CASCADE)
    especie = models.ForeignKey(Especie, on_delete=models.CASCADE)
    sexo = models.ForeignKey(Sexo, on_delete=models.CASCADE)
    
    observaciones = models.TextField(null=True, blank=True)
    imagen = models.ImageField(null=True, blank=True)
    nombreContacto = models.CharField(null=True, max_length=200)
    telefonoContacto = models.CharField(null=True, max_length=12)
    

