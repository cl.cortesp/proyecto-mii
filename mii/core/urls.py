from django.urls import path
from .views import home, nosotros, registrar, encontradas, perdidas, listadoMascotas, modificar_mascota, eliminar_mascota, registro, perfil_encontrada, perfil_perdida

urlpatterns = [
    path('', home, name="home"),
    path('nosotros/', nosotros, name=("nosotros")),
    path('agregar_mascota/', registrar, name="registrar"),
    path('encontradas/', encontradas, name="galeria"),
    path('perdidas/', perdidas, name="perdidas"),
    path('listadoMascotas/', listadoMascotas, name="listado"),
    path('modificar_mascota/<id>/', modificar_mascota, name="modificar"),
    path('eliminar_mascota/<id>/', eliminar_mascota, name="eliminar"),
    path('registro/', registro, name='registro_usuario'),
    path('encontrada/<id>/', perfil_encontrada, name='perfil_e'),
    path('perdida/<id>/', perfil_perdida, name="perfil_p"),
]