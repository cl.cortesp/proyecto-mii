from django.shortcuts import render, redirect
from .models import Mascota
from .forms import MascotaForm, CustomUserForm
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import login, authenticate



# Create your views here.

def home(request):
    return render(request, 'core/home.html')

def nosotros(request):
    return render(request, 'core/nosotros.html')

@login_required
def registrar(request):
    data = {
        'form':MascotaForm()
    }

    if request.method == 'POST':
        formmulario = MascotaForm(request.POST, files=request.FILES)
        if formmulario.is_valid():
            formmulario.save()
            data['mensaje'] = "Guardado correctamente"

    return render(request, 'core/agregar_mascota.html', data)

def encontradas(request):
    
    data = {
        'encontrada': Mascota.objects.filter(tipo_id=1)
    }
    
    return render(request, 'core/encontradas.html', data)

def perdidas(request):
    data = {
        'perdida': Mascota.objects.filter(tipo_id=2)
    }

    return render(request, 'core/perdidas.html', data) 

@permission_required('core.delete_mascota')
def listadoMascotas(request):
    mascotas = Mascota.objects.all()
    
    data = {
        'listado': mascotas
    }

    return render(request, 'core/listadoMascotas.html', data)

def modificar_mascota(request, id):
    mascota = Mascota.objects.get(id=id)

    data = {
        'form': MascotaForm(instance=mascota)
    }

    if request.method == 'POST':
        formulario = MascotaForm(data=request.POST, instance=mascota, files=request.FILES)
        
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Información modificada correctamente"
            data['form'] = MascotaForm(instance=Mascota.objects.get(id=id))

    return render(request, 'core/modificar_mascota.html', data)

def eliminar_mascota(request, id):
    mascota = Mascota.objects.get(id=id)
    mascota.delete()

    return redirect(to = "listado")

def registro(request):
    data = {
        'form': CustomUserForm
    }

    if request.method == 'POST':
        formulario = CustomUserForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            username = formulario.cleaned_data['username']
            password = formulario.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect(to='home')
    return render(request, 'registration/registrar.html', data)

def perfil_encontrada(request, id):
    mascota = Mascota.objects.get(id=id)
    data = {
        'perfil': mascota
    }
    return render(request, 'core/perfil_encontrada.html', data)

def perfil_perdida(request, id):
    mascota = Mascota.objects.get(id=id)
    data = {
        'perfil': mascota
    }
    return render(request, 'core/perfil_perdida.html', data)
